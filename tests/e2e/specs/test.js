// https://docs.cypress.io/api/introduction/api.html

describe('Looking for app data', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('h3', 'Festival List')

    cy
      .get('.ea-test')
      .should('be.visible')
  })
})
