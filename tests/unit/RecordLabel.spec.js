import { mount } from '@vue/test-utils'
import { config } from '@vue/test-utils'
config.showDeprecationWarnings = false
import RecordLabel from "@/components/RecordLabel.vue"

describe('Record Label is loaded with data', () => {
    //Arrange
    let wrapper;
    beforeEach( () => {
      wrapper = mount(RecordLabel, {
        methods: {fetchDataFromAPI: () => {} }
      })
    })
    //Assert
    it("Rendered", ()=> {
      expect( wrapper.exists()).toBe(true);
    })
    it("Parent DIV is visible", ()=> {
      expect(wrapper.find('.ea-test').isVisible()).toBe(true)
    })
    it('renders a H3 with value: Festival List', () => {
      expect(wrapper.find("h3").text("Festival List"));
    })
    it('Rendered the UL', () => {
      expect(wrapper.find("#recordLabel").isVisible()).toBe(true)
    })

  })


//Other test we can do
//Test API methods
//Mock json data
//in a real world application, end to end test can be 
